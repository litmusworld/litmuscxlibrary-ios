#
# Be sure to run `pod lib lint LitmusCXLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'LitmusCXLibrary'
s.version          = '0.3.3'
s.summary          = 'LitmusCXLibrary is iOS library to add Litmus Conversation into their app.'
s.swift_version    = '4.0'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
LitmusCXLibrary is iOS library to add Litmus Conversation into their app.
DESC

s.homepage         = 'https://bitbucket.org/litmusworld/litmuscxlibrary-ios'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
s.author           = { 'anubhavgiri' => 'anubhav.giri@litmusworld.com' }
s.source           = { :git => 'https://litmusworld@bitbucket.org/litmusworld/litmuscxlibrary-ios.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '11.0'

s.source_files = 'LitmusCXLibrary/Classes/**/*'

s.resource_bundles = {
'LitmusCXLibrary' => ['LitmusCXLibrary/Assets/**/*']
}

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'

s.dependency 'Alamofire', '~> 5.8.1'
s.dependency 'SwiftyJSON', '~> 5.0'


end
