//
//  LitmusCXView.swift
//  Pods
//
//  Created by admin on 31/10/17.
//
//

import UIKit
import WebKit


@objc public protocol LitmusCXDelegate {
    
    func onClose()
}

let DEFAULT_SHARE_ENABLED : Bool = false
let DEFAULT_COPY_ENABLED : Bool = false
let DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE : Bool = true

open class LitmusCXView: UIView, UIWebViewDelegate, LitmusMoreOptionsDelegate, WKUIDelegate {

    @IBOutlet weak var litmusWebView: WKWebView!
    // @IBOutlet var litmusWebView: WKWebView!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var reloadView: UIView!
    
    @IBOutlet weak var reloadImageView: UIImageView!
    
    open var notificationPayLoad : AnyObject?
    open var strWebUrl : String?
    open var strFeedbackUrl : String?
    
    var strBaseUrl : String?
    //var window: UIWindow?
    let defaults = UserDefaults.standard
    
    open var delegate : LitmusCXDelegate?
    var  moreOptionsDelegate : LitmusMoreOptionsDelegate?
    
    var oTagParameters : [String:Any]?
    
    var isMoreImageBlackElseWhite : Bool = DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE
    var isShareEnabled : Bool = DEFAULT_SHARE_ENABLED
    var isCopyEnabled : Bool = DEFAULT_COPY_ENABLED
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
//        fnInitialize()
        
    }
    
    
    
    @IBAction func moreButtonClicked(_ sender: AnyObject) {
        self.fnOpenMoreOptions(isShareEnabled, isCopyEnabled: isCopyEnabled, view: self)
    }
    
    func fnClose() {
//        var superview = self.superview
//        
//        var isContinue = true
//        
//        while isContinue {
//            
//            if superview == nil {
//                isContinue = false
//                break
//            }
//            
//            if superview! as? UIViewController != nil {
//                (superview as! UIViewController).dismiss(animated: false, completion: nil)
//                isContinue = false
//            }
//            
//            
//            if superview! as? LitmusKLCPopup != nil {
//                (superview as! LitmusKLCPopup).dismissPresentingPopup()
//                isContinue = false
//            }
//            
//            superview = superview?.superview
//        }
        
        
        
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            
            if let viewController = parentResponder as? UIViewController {
                
                viewController.dismiss(animated: false, completion: nil)
                break
            }
            
            if let popup = parentResponder as? LitmusKLCPopup {
                popup.dismissPresentingPopup()
                break
            }


        }
        
        
        if delegate != nil {
            delegate!.onClose()
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    open func fnInitialize() {
        
        if notificationPayLoad != nil {
            //Set default values received from the notification.
            makeDefault(notificationPayLoad!)
            
            if notificationPayLoad!["base_url"] != nil {
                strBaseUrl = notificationPayLoad!["base_url"] as? String
            }
            
            if notificationPayLoad!["tag_parameters"] != nil {
                oTagParameters = notificationPayLoad!["tag_parameters"] as? [String:AnyObject]
            }
            
            if notificationPayLoad!["is_more_image_black_else_white"] != nil {
                isMoreImageBlackElseWhite = notificationPayLoad!["is_more_image_black_else_white"] as! Bool
            }
            
            if notificationPayLoad!["is_share_enabled"] != nil {
                isShareEnabled = notificationPayLoad!["is_share_enabled"] as! Bool
            }
            
            if notificationPayLoad!["is_copy_enabled"] != nil {
                isCopyEnabled = notificationPayLoad!["is_copy_enabled"] as! Bool
            }
        }
            
        setUpLitmusRater()
    }
    
    func setUpLitmusRater() {
        
        //Once the view is loaded. Show loading icon.
        displayLitmusLoadingPage()
        
        // Setup More Image
        fnSetMoreImage(self.isMoreImageBlackElseWhite)
        
        if notificationPayLoad != nil {
            //After displaying loading. Get link and redirect.
            getLitmusLinkAndOpen()
        } else if strWebUrl != nil && strWebUrl!.count > 0 {
            openWebViewWithUrl(strWebUrl!)
        }
    }
    
    public static func fnOpenLitmusFeedback(_ strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, viewController : UIViewController) {
        
        fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters : nil, isMoreImageBlackElseWhite : DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled : DEFAULT_COPY_ENABLED, isShareEnabled: DEFAULT_SHARE_ENABLED, viewController: viewController);
    }
    
    public static func fnOpenLitmusFeedback(_ strBaseUrl : String?, strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, tagParameters : [String:Any]?, isMoreImageBlackElseWhite : Bool, isCopyEnabled : Bool, isShareEnabled : Bool, viewController : UIViewController) {
        
        
        let notificationPayLoad = fnCreateNotificationPayload(strBaseUrl,
                                                                                   strAppId : strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: tagParameters, isMoreImageBlackElseWhite: isMoreImageBlackElseWhite, isCopyEnabled: isCopyEnabled,
                                                                       isShareEnabled: isShareEnabled)
        
    }
    
    public static func fnGetLitmusFeedbackView(_ notificationPayLoad : AnyObject) -> LitmusCXView {
        //Load the litmusView Controller with web view.
        return fnGetLitmusFeedbackView(notificationPayLoad, delegate: nil)
    }

    public static func fnGetDefaultLitmusCXView(_ delegate : LitmusCXDelegate?) -> LitmusCXView {
        let podBundle = Bundle(for: self.classForCoder())
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        let nib = UINib(nibName:"LitmusCXView", bundle:frameWorkBundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! LitmusCXView
        view.delegate = delegate
        
        // Set reload image
        let strReloadImageName = "litmus_reload.png"
        let reloadImage = UIImage(named: strReloadImageName, in: frameWorkBundle, compatibleWith: nil)
        
        view.reloadImageView.image = reloadImage
        
        // Reloadview tap gesture
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: view, action: #selector(self.handleReloadViewTap(_:)))
        view.reloadView.addGestureRecognizer(tap)
        view.reloadView.isUserInteractionEnabled = true
        
        // view.reloadView.target(forAction: #selector(handleReloadViewTap(_:), withSender: view)
        
        return view
    }
    
    public static func fnGetLitmusFeedbackView(_ notificationPayLoad : AnyObject, delegate : LitmusCXDelegate?) -> LitmusCXView {
        //Load the litmusView Controller with web view.
        let view = fnGetDefaultLitmusCXView(delegate)
        
        view.notificationPayLoad = notificationPayLoad
        view.fnInitialize()
        
        return view
        
        
        //        view.frame = bounds
        //        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.addSubview(view);
    }
    
    
    public static func fnGetLitmusWebUrlView(_ strWebUrl : String, delegate : LitmusCXDelegate?) -> LitmusCXView {
        //Load the litmusView Controller with web view.
        return fnGetLitmusWebUrlView(strWebUrl, isMoreImageBlackElseWhite: DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled: DEFAULT_COPY_ENABLED, isShareEnabled: DEFAULT_SHARE_ENABLED, delegate: delegate)
    }
    
    
    public static func fnGetLitmusWebUrlView(_ strWebUrl : String, isMoreImageBlackElseWhite : Bool, isCopyEnabled : Bool, isShareEnabled : Bool, delegate : LitmusCXDelegate?) -> LitmusCXView {
        //Load the litmusView Controller with web view.
        let view = fnGetDefaultLitmusCXView(delegate)
        
        view.strWebUrl = strWebUrl
        view.isMoreImageBlackElseWhite = isMoreImageBlackElseWhite
        view.isCopyEnabled = isCopyEnabled
        view.isShareEnabled = isShareEnabled
        
        view.fnInitialize()
        
        return view
        
        
        //        view.frame = bounds
        //        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.addSubview(view);
    }

    
    
    public static func fnCreateNotificationPayload(_ strBaseUrl : String?, strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, tagParameters : [String:Any]?, isMoreImageBlackElseWhite : Bool, isCopyEnabled : Bool, isShareEnabled : Bool) -> [String : AnyObject?] {
        var notificationPayLoad : [String : AnyObject] = [:]
        notificationPayLoad["app_id"] = strAppId as AnyObject?
        notificationPayLoad["reminder_number"] = nReminderNumber as AnyObject?
        notificationPayLoad["customer_id"] = strUserId as AnyObject?
        notificationPayLoad["user_email"] = strUserEmail as AnyObject?
        notificationPayLoad["name"] = strUserName as AnyObject?
        notificationPayLoad["allow_multiple_feedbacks"] = isAllowMultipleFeedbacks as AnyObject?
        
        if strBaseUrl != nil {
            notificationPayLoad["base_url"] = strBaseUrl as AnyObject?
        }
        
        if tagParameters != nil {
            notificationPayLoad["tag_parameters"] = tagParameters as AnyObject?
        }
        
        notificationPayLoad["is_more_image_black_else_white"] = isMoreImageBlackElseWhite as AnyObject?
        
        notificationPayLoad["is_share_enabled"] = isShareEnabled as AnyObject?
        notificationPayLoad["is_copy_enabled"] = isCopyEnabled as AnyObject?
        
        //notificationPayLoad["source"] = "litmus"
        //notificationPayLoad["alert"] = "Message"
        //notificationPayLoad["title"] = "Feedback"
        return notificationPayLoad
    }
    
    
    //Persist data some where to get it later when needed.
    func makeDefault(_ notificationPayLoad : AnyObject)
    {
        let appId =  notificationPayLoad["app_id"];
        let reminderNumber =  notificationPayLoad["reminder_number"];
        let userEmail =  ""//notificationPayLoad["user_email"];
        let customerId =  notificationPayLoad["customer_id"];
        
        defaults.set(appId, forKey: "notification_appId")
        defaults.set(reminderNumber, forKey: "notification_reminderNumber")
        defaults.set(userEmail, forKey: "userEmail")
        defaults.set(customerId, forKey: "customerId")
    }
    
    //Load status HTML page for loading icon.
    func displayLitmusLoadingPage() {
        
        // Importatnt to intercept local image in this protocol and load it in local html
        URLProtocol.registerClass(NSURLProtocolCustom.classForCoder())
        
        //Get local html file
        
//        NSString *path = [[NSBundle mainBundle] bundlePath];
//        NSURL *baseURL = [NSURL fileURLWithPath:path];
//        [webView loadHTMLString:htmlString baseURL:baseURL];
        
        
        let podBundle = Bundle(for: self.classForCoder)
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        let localfilePath = frameWorkBundle?.url(forResource: "litmus", withExtension: "html");
        //Create request for URL.
        let myRequest = URLRequest(url: localfilePath!);
        //Define a tag so web view can be closed if needed.
        litmusWebView.tag = ConfigurationConstant.WEB_VIEW_TAG;
        //Load static page on web view.
        litmusWebView.load(myRequest)
                
        // Load from string
//        let path = frameWorkBundle?.path(forResource: "litmus", ofType: "html")
//        let strHtml = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
//        litmusWebView.loadHTMLString(strHtml!, baseURL: localfilePath)
        
        
        // hide reload view
        self.reloadView.isHidden = true
        self.litmusWebView.isHidden = false
    }
    
    //Function to call and get the URL from Litmus Server and Change the webview URL.
    func getLitmusLinkAndOpen()
    {
        //Get appId & reminderNumber from the cache if available.
        let notificationAppId = defaults.object(forKey: "notification_appId") as! String?
        let notificationReminderNumber = defaults.object(forKey: "notification_reminderNumber") as! NSNumber?
        
        let isAllowMultipleFeedbacks = self.notificationPayLoad!["allow_multiple_feedbacks"] as! Bool
        let strCustomerName = self.notificationPayLoad!["name"] as! String
        
        
        //Get Email from cache
        let userEmail = defaults.object(forKey: "userEmail") as! String?
        
        //If only appId and emailId is availble.
        if(notificationAppId != nil && notificationReminderNumber != nil){
            //Check if we have longUrl for same appId and reminderNumber
            let appId = defaults.object(forKey: "appId") as! String?
            let reminderNumber = defaults.object(forKey: "reminderNumber") as! NSNumber?
            let longUrl = defaults.object(forKey: "longUrl") as! String?
            let customerId = defaults.object(forKey: "customerId") as! String?
            
            if(appId != nil && reminderNumber != nil && longUrl != nil && reminderNumber != -1) {
                if(appId == notificationAppId && reminderNumber == notificationReminderNumber){
                    if(longUrl != nil){
                        openWebViewWithUrl(longUrl!)
                    }
                    else{
                        generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
                    }
                }
                else{
                    generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
                }
            } else{
                generateURL(notificationAppId, userEmail: userEmail, reminderNumber : notificationReminderNumber, customerId: customerId, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strCustomerName)
            }
        }
    }
    
    
    //Generate URL based on appId && userEmail
    func generateURL(_ appId : String!, userEmail : String!, reminderNumber : NSNumber!, customerId : String!, isAllowMultipleFeedbacks : Bool, strUserName : String) {
        
//        var tagParameters = [String:Any]()
//        //Configure Post request body.
//        tagParameters["TAG_TEST"] = "Testing"
//        tagParameters["TAG_TIME"] = 123
        
        let restAPIManger = RestApiManager()
        
        //Call API and get Long URL.
        restAPIManger.getLitmusURL(strBaseUrl, appId: appId!, notifyChannel: ConfigurationConstant.NOTIFY_CHANNEL, userEmail: userEmail! , customerId :customerId!, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, strUserName: strUserName, oTagParameters: oTagParameters) { (long_url : String) in
            if ((long_url) == "No URL") {
                //If URL is not available Or Some Error. provide Reload option.
                self.onFailure()
                
            } else if ((long_url) == "Thank you for your interest. You have already submitted your feedback.") {
              
                DispatchQueue.main.async {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        self.fnClose()
                        let alert = UIAlertController(title: "", message: long_url, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                            case .cancel:
                                print("cancel")
                            case .destructive:
                                print("destructive")
                            }}))
                        
                        topController.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else {
                //Once Long URL is generated. Push appId, remidernumber and long_url to cache. So that it can be used later.
                self.defaults.set(long_url, forKey: "longUrl")
                self.defaults.set(reminderNumber, forKey: "reminderNumber")
                self.defaults.set(appId, forKey: "appId")
                
                self.openWebViewWithUrl(long_url);
            }
        }
    }
    
    
    //Open link in web view.
    func openWebViewWithUrl(_ long_url : String) {
        
        self.strFeedbackUrl = long_url
        
//        self.litmusWebView.delegate = self
        self.litmusWebView.uiDelegate = self
        
        let url = URL (string: long_url);
        let requestObj = URLRequest(url: url!);
        self.litmusWebView.load(requestObj);
    }
    
    open func webViewDidFinishLoad(_ webView : UIWebView) {
        
        //        print(webView.request!.URL!.absoluteString)
        
        self.moreButton.setTitleColor(UIColor.white, for: UIControl.State())
    
    }
    
    open func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        // additional check for request being made
        if error as? NSError != nil {
            if (error as NSError).code != NSURLErrorCancelled {
                self.onFailure()
            }
        }
        
    }
    
    open func fnHideMoreButton(_ isHidden : Bool) {
        
        if self.moreButton != nil {
            self.moreButton.isHidden = isHidden
        }
    }
    
    // function which is triggered when handleReloadViewTap is called
//    @objc
   @objc func handleReloadViewTap(_ sender: UITapGestureRecognizer) {
        fnInitialize()
    }
    
    func onFailure() {
        // show reload view
        self.reloadView.isHidden = false
        self.litmusWebView.isHidden = true
    }

    // MARK: Set More Image
    func fnSetMoreImage(_ isMoreImageBlackElseWhite : Bool) -> Void {
        let podBundle = Bundle(for: self.classForCoder)
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        var strMoreImageName : String?;
        if isMoreImageBlackElseWhite {
            strMoreImageName = "ic_more_vert_black.png"
        } else {
            strMoreImageName = "ic_more_vert_white.png"
        }
        
        let moreImage = UIImage(named: strMoreImageName!, in: frameWorkBundle, compatibleWith: nil)
        

        //---- disabling for Air Asia client only
            // self.moreButton.setImage(moreImage, for: UIControl.State.normal)
            
        //----- enabling for Air Asia client only
            self.moreButton.setImage(nil, for: UIControl.State())
            self.moreButton.setTitle("X", for: .normal)
            self.moreButton.setTitleColor(.black, for: .normal)
        //----

    }
    
    // MARK: Open More options
    func fnOpenMoreOptions(_ isShareEnabled : Bool, isCopyEnabled : Bool, view : UIView) {
    
    //----- enabling for Air Asia client only
        onCloseItemClicked()
    
        
    //---- disabling for Air Asia client only
    //-- Remove multiline comment to enable the code for all the clients
    /*
        var delegate : LitmusMoreOptionsDelegate?
        
        if view as? LitmusMoreOptionsDelegate != nil {
            delegate = view as! LitmusMoreOptionsDelegate
        }
        
        // Workaround : Created dummy More Options View Controller, as it is displayed behind the LitmusKLCPopup in some cases
//        let dummyLitmusMoreOptionsViewController = LitmusMoreOptionsViewController.fnGetMoreOptionsViewController(isShareEnabled, isCopyEnabled: isCopyEnabled, delegate: delegate)
//        dummyLitmusMoreOptionsViewController.showPopover(base: moreButton)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            // Fix: Dummy Options View controller is added since, on first launch it is hidden behind LitmusKLCPopup
//            dummyLitmusMoreOptionsViewController.dismiss(animated: false, completion: nil)
//        }
        
        let litmusMoreOptionsViewController = LitmusMoreOptionsViewController.fnGetMoreOptionsViewController(isShareEnabled, isCopyEnabled: isCopyEnabled, delegate: delegate)

        litmusMoreOptionsViewController.showPopover(base: moreButton) */
    }
    
    // MARK: LitmusMoreOptionsViewDelegate methods
    public func onCloseItemClicked() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.fnClose()
        }
        
        
    }
    
    public func onShareItemClicked() {
        
        let currentURL : String? = fnGetCurrentLoadedUrl()
        
        if currentURL != nil && currentURL!.count > 0 {
            let text = currentURL!
            let textShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                var currentViewController = LitmusCXView.getCurrentViewController(self)
                
                if currentViewController == nil {
                    currentViewController = LitmusCXView.topMostController()
                }
                
                if currentViewController != nil {
                    currentViewController!.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    public func onCopyItemClicked() {
        
        let currentURL : String? = fnGetCurrentLoadedUrl()
        
        UIPasteboard.general.string = currentURL
        
//        let mainView : UIView = UIView()
        
        let labelView : UILabel = UILabel()
        labelView.text = "Link Copied"
        labelView.textColor = UIColor.darkText
        labelView.sizeToFit()
//        labelView.layoutMargins = UIEdgeInsetsMake(5, 5, 5, 5)
        labelView.backgroundColor = UIColor.white.withAlphaComponent(0.85)
        
//        mainView.addSubview(labelView)
//        mainView.backgroundColor = UIColor.white.withAlphaComponent(0.85)
        
        let popUp = LitmusKLCPopup()
        
        popUp.contentView = labelView
        popUp.dimmedMaskAlpha = 0 // No dimming background
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            popUp.show(withDuration: 2)
        }
    }
    
    //MARK: Utility Method
    
    public static func topMostController() -> UIViewController? {
        
        var topController : UIViewController? = nil
        
        if UIApplication.shared.keyWindow != nil {
           topController = UIApplication.shared.keyWindow?.rootViewController
        }
        
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        
        return topController
    }
    
    func fnGetCurrentLoadedUrl() -> String {
        
        var strCurrentUrl = ""
        
        if strFeedbackUrl != nil && strFeedbackUrl!.count > 0 {
            strCurrentUrl = strFeedbackUrl!
        } else if strWebUrl != nil && strWebUrl!.count > 0 {
            strCurrentUrl = strWebUrl!
        }
        
        return strCurrentUrl
    }
    
    public static func getCurrentViewController(_ currentView : UIView) -> UIViewController? {
        
        var currentViewController : UIViewController? = nil

        var parentResponder: UIResponder? = currentView
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                currentViewController = viewController
                break
            }
        }

        return currentViewController
    }
    
    public static func getCurrentLitmusKLCPopUpView(_ currentView : UIView) -> LitmusKLCPopup? {
        
        var currentLitmusKLCPopupView : LitmusKLCPopup? = nil
        
        var parentView: UIView? = currentView
        while parentView != nil {
            parentView = parentView!.superview
            if let view = parentView as? LitmusKLCPopup {
                currentLitmusKLCPopupView = view
                break
            }
        }
        
        return currentLitmusKLCPopupView
    }
    
}
