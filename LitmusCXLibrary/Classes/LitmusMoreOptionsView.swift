//
//  LitmusMoreOptionsView.swift
//  LitmusCXLibrary
//
//  Created by admin on 13/12/17.
//

import UIKit

@objc public protocol LitmusMoreOptionsDelegate {
    
    func onCloseItemClicked()
    func onShareItemClicked()
    func onCopyItemClicked()
}

class LitmusMoreOptionsView: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var isShareEnabled : Bool = false
    var isCopyEnabled : Bool = false
    var delegate : LitmusMoreOptionsDelegate?
    
    @IBOutlet weak var moreTableView: UITableView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public static func fnGetMoreOptionsView(_ isShareEnabled : Bool, isCopyEnabled : Bool, delegate : LitmusMoreOptionsDelegate?) -> LitmusMoreOptionsView {
        let podBundle = Bundle(for: self.classForCoder())
        let bundleURL = podBundle.url(forResource: "LitmusCXLibrary", withExtension: "bundle")
        let frameWorkBundle = Bundle(url: bundleURL!)
        
        let nib = UINib(nibName:"LitmusMoreOptionsView", bundle:frameWorkBundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! LitmusMoreOptionsView
        
        
        view.fnInitialize(isShareEnabled, isCopyEnabled: isCopyEnabled)
        view.delegate = delegate
        
        return view
    }
    

    //
    func fnInitialize(_ isShareEnabled : Bool, isCopyEnabled : Bool) -> Void {
        
        self.isShareEnabled = isShareEnabled
        self.isCopyEnabled = isCopyEnabled
    
        
        self.moreTableView.delegate = self
        self.moreTableView.dataSource = self
        
    }
    
    
    // MARK:- UITableViewDataSource delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var nNumberOfSections : Int = 1
        
        if isShareEnabled {
            nNumberOfSections = nNumberOfSections + 1
        }
        
        if isCopyEnabled {
            nNumberOfSections = nNumberOfSections + 1
        }
        
        return nNumberOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "MyTestCell")
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MyTestCell")
        
        var strTitle : String?
        var strSubTitle : String?
        
        let nRowNumber = indexPath.row
        
        switch nRowNumber {
            case 0:
                if isCopyEnabled {
                    strTitle = "Copy Link"
                } else if isShareEnabled {
                    strTitle = "Share Link"
                } else {
                    strTitle = "Close"
                }
                
                break
            
            case 1:
                if isCopyEnabled {
                    if isShareEnabled {
                        strTitle = "Share Link"
                    } else {
                        strTitle = "Close"
                    }
                } else {
                    strTitle = "Close"
                }
                
                break
            
            case 2:
                // All 3 are enabled, so last item on close
                strTitle = "Close"
                
                break
            
            default:
                strTitle = "Row #\(indexPath.row)"
        }
        
        cell.textLabel?.text = strTitle
//        cell.detailTextLabel?.text = "Subtitle #\(indexPath.row)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nRowNumber = indexPath.row
        
        switch nRowNumber {
            case 0:
                if isCopyEnabled {
                    if delegate != nil {
                        delegate?.onCopyItemClicked()
                    }
                    
                    
                } else if isShareEnabled {
                    if delegate != nil {
                        delegate?.onShareItemClicked()
                    }
                } else {
                    if delegate != nil {
                        delegate?.onCloseItemClicked()
                    }
                }
            
            break
            
        case 1:
            if isCopyEnabled {
                if isShareEnabled {
                    if delegate != nil {
                        delegate?.onShareItemClicked()
                    }
                } else {
                    if delegate != nil {
                        delegate?.onCloseItemClicked()
                    }
                }
            } else {
                if delegate != nil {
                    delegate?.onCloseItemClicked()
                }
            }
            
            break
            
        case 2:
            // All 3 are enabled, so last item on close
            if delegate != nil {
                delegate?.onCloseItemClicked()
            }
            
            break
            
        default:
            
            break;
        }
        
        fnClose()
        
    }
    
    // MARK: Closes More Options View
    
    func fnClose() {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            
            if let popup = parentResponder as? LitmusKLCPopup {
                
                popup.dismissPresentingPopup()
                break
            } else if let popup = parentResponder as? UIViewController {
                popup.dismiss(animated: false, completion: nil)
                break
            }
        }
    }
    
    
}
