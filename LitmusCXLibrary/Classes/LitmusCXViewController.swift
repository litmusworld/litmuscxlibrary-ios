//
//  LitmusCXViewController.swift
//  Pods
//
//  Created by admin on 01/11/17.
//
//

import UIKit

open class LitmusCXViewController: UIViewController, LitmusCXDelegate {

    open var notificationPayLoad : AnyObject?
//    @objc
    open var strWebUrl : String?
    
    open var delegate : LitmusCXDelegate?
    
    var litmusCxView : LitmusCXView?
    
    var isHideCloseButton : Bool = false
    
    var isMoreImageBlackElseWhite : Bool = DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE
    var isShareEnabled : Bool = DEFAULT_SHARE_ENABLED
    var isCopyEnabled : Bool = DEFAULT_COPY_ENABLED
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.notificationPayLoad != nil {
            litmusCxView = LitmusCXView.fnGetLitmusFeedbackView(self.notificationPayLoad!, delegate: self)
        } else if self.strWebUrl != nil && self.strWebUrl!.count > 0 {
            litmusCxView = LitmusCXView.fnGetLitmusWebUrlView(self.strWebUrl!, isMoreImageBlackElseWhite: isMoreImageBlackElseWhite, isCopyEnabled: isCopyEnabled, isShareEnabled: isShareEnabled, delegate: self)
        }
        
        if litmusCxView != nil {
            self.view.addSubview(litmusCxView!)
            
            // topmargin 20 added to Avoid overlapping of view with top status bar
            self.fnPinToSuperView(litmusCxView!, topMargin: 20, rightMargin: 0, bottomMargin: 0, leftMargin: 0)
            
            self.fnHideMoreButton(isHideCloseButton)
        }
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    @objc
    open func fnHideMoreButton(_ isHidden : Bool) {
        
        isHideCloseButton = isHidden
        
        if litmusCxView != nil {
            litmusCxView!.fnHideMoreButton(isHidden)
        }
    }
    
//    @objc
    public static func fnInitAndGetViewController(_ viewController : UIViewController?) -> LitmusCXViewController {
        
        // commented as it is get from storyboard
        let litmusViewController : LitmusCXViewController = LitmusCXViewController()
        
        var delegate : LitmusCXDelegate?
        
        if viewController != nil && viewController as? LitmusCXDelegate != nil {
            delegate = viewController as! LitmusCXDelegate
        }
        
        litmusViewController.delegate = delegate
        
        return litmusViewController
    }
    
    
//    @objc
    public static func fnOpenLitmusFeedback(_ strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, viewController : UIViewController) {
        
        fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: nil, isMoreImageBlackElseWhite : DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled : DEFAULT_COPY_ENABLED, isShareEnabled : DEFAULT_SHARE_ENABLED, viewController: viewController);
    }
    
//    @objc
    public static func fnOpenLitmusFeedback(_ strBaseUrl : String?, strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, tagParameters : [String:Any]?,isMoreImageBlackElseWhite : Bool,isCopyEnabled : Bool, isShareEnabled: Bool, viewController : UIViewController) {
        
        
        let notificationPayLoad = LitmusCXView.fnCreateNotificationPayload(strBaseUrl,
                                                                                   strAppId : strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: tagParameters,
                                                                               isMoreImageBlackElseWhite: isMoreImageBlackElseWhite, isCopyEnabled: isCopyEnabled, isShareEnabled: isShareEnabled)
        
        let litmusViewController = fnInitAndGetViewController(viewController)
        
        litmusViewController.notificationPayLoad = notificationPayLoad as AnyObject?
        
        viewController.present(litmusViewController, animated: false, completion: nil)
    }
    
//    @objc
    public static func fnOpenWebUrl(_ strWebUrl : String, viewController : UIViewController) {
        fnOpenWebUrl(strWebUrl, isMoreImageBlackElseWhite: DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled: DEFAULT_COPY_ENABLED, isShareEnabled: DEFAULT_SHARE_ENABLED, viewController: viewController)
    }
    
//    @objc
    public static func fnOpenWebUrl(_ strWebUrl : String, isMoreImageBlackElseWhite : Bool, isCopyEnabled: Bool, isShareEnabled : Bool , viewController : UIViewController) {
        
        let litmusViewController = fnInitAndGetViewController(viewController)
        litmusViewController.strWebUrl = strWebUrl
        litmusViewController.isMoreImageBlackElseWhite = isMoreImageBlackElseWhite
        litmusViewController.isCopyEnabled = isCopyEnabled
        litmusViewController.isShareEnabled = isShareEnabled
        
        viewController.present(litmusViewController, animated: false, completion: nil)
    }
    
    
    func fnPinToSuperView(_ viewTobePinned : UIView, topMargin : CGFloat, rightMargin : CGFloat, bottomMargin : CGFloat, leftMargin : CGFloat) {
        
        viewTobePinned.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: viewTobePinned,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: viewTobePinned.superview,
                                               attribute: .top,
                                               multiplier: 1,
                                               constant: topMargin)
        
        let trailingConstraint = NSLayoutConstraint(item: viewTobePinned,
                                                    attribute: .trailing,
                                                    relatedBy: .equal,
                                                    toItem: viewTobePinned.superview,
                                                    attribute: .trailing,
                                                    multiplier: 1,
                                                    constant: -rightMargin)
        
        let bottomConstraint = NSLayoutConstraint(item: viewTobePinned,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: viewTobePinned.superview,
                                                  attribute: .bottom,
                                                  multiplier: 1,
                                                  constant: -bottomMargin)
        
        let leadingConstraint = NSLayoutConstraint(item: viewTobePinned,
                                                    attribute: .leading,
                                                    relatedBy: .equal,
                                                    toItem: viewTobePinned.superview,
                                                    attribute: .leading,
                                                    multiplier: 1,
                                                    constant: leftMargin)
        
        viewTobePinned.superview?.addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    
    // MARK: - LitmusCXDelegate Method
    public func onClose() {
        
        if(isUploadControllerVisible) {
            isUploadControllerVisible = false
            self.dismiss(animated: false, completion: nil)
        }
        
        if delegate != nil {
            delegate!.onClose()
        }
    }
    
    // Workaround, to dismiss - File upload popup not dismissing current view controller popup
    open override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if self.presentedViewController != nil {
            super.dismiss(animated: flag, completion: completion)
        } else if !isUploadControllerVisible {
            super.dismiss(animated: flag, completion: completion)
        }
    }
    
    // Workaround, to dismiss - File upload popup not dismissing current view controller popup
    var isUploadControllerVisible = false
    
    // Workaround, to dismiss - File upload popup not dismissing current view controller popup
    open override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        // Avoiding iOS bug. UIWebView with file input doesn't work in modal view controller
        if viewControllerToPresent.isKind(of: UIDocumentMenuViewController.self) || viewControllerToPresent.isKind(of: UIImagePickerController.self) {
            isUploadControllerVisible = true
        }
        
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
}
