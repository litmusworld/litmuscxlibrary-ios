/* Copyright (C) Litmusworld Pvt Ltd - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Kapil Dave <kapil@litmusworld.com>, August 2016
 */

import SwiftyJSON
import Alamofire


typealias ServiceResponse = (String) -> Void

class RestApiManager: NSObject {
    static let sharedInstance = RestApiManager()
    
    
    func getLitmusURL(_ strBaseUrl : String?, appId : String, notifyChannel : String,  userEmail : String, customerId:  String, isAllowMultipleFeedbacks : Bool, strUserName : String, oTagParameters : [String:Any]?, onCompletion: @escaping (String) -> Void) {
        var parameters = [String:Any]()
        //Configure Post request body.
        parameters[ConfigurationConstant.APP_ID_FIELD] = appId
        parameters[ConfigurationConstant.NOTIFICATION_FIELD] = notifyChannel
        parameters[ConfigurationConstant.EMAIL_ID_FIELD] = userEmail
        parameters[ConfigurationConstant.CUSTOMER_ID_FIELD] = customerId
        parameters[ConfigurationConstant.ALLOW_MULTIPLE_FEEDBACK_FIELD] = isAllowMultipleFeedbacks
        parameters[ConfigurationConstant.NAME_FIELD] = strUserName
        parameters[ConfigurationConstant.TAG_OS_FIELD] = "iOS"
        parameters[ConfigurationConstant.TAG_USERID_FIELD] = customerId
        
        if oTagParameters != nil {
            for(key, value) in oTagParameters! {
                parameters[key] = value
            }
        }
        
        makeHTTPPostRequest(strBaseUrl, parameters: parameters  , onCompletion: { long_url in
            onCompletion(long_url as String)
        })
    }
    
    
    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ strBaseUrl : String?, parameters: [String: Any], onCompletion: @escaping ServiceResponse) {
        
        var strServerUrl : String?;
        
        if strBaseUrl != nil && strBaseUrl!.count > 0 {
            
            if strBaseUrl!.hasSuffix("/") {
                strServerUrl = strBaseUrl! + ConfigurationConstant.GENERATE_FEEDBACK_URL
            } else {
                strServerUrl = strBaseUrl! + "/" + ConfigurationConstant.GENERATE_FEEDBACK_URL
            }
        }
        
        if strServerUrl == nil {
            strServerUrl = ConfigurationConstant.GENERATE_URL_REQUEST
        }
        
        let request = AF.request(strServerUrl!, method: .post, parameters: parameters)
        
        request.responseJSON { response in
            //Check if error is nil or not
            if (response.error != nil)
            {
                print("Error : \(response.error.debugDescription)")
                //TODO Error Handling for the Server Response
                onCompletion("No URL")
            }
                //If Response is 200 then parse the data.
            else if response.response!.statusCode == 200{
                let response = JSON(response.value!)
                if(response["data"]["code"] == 0){
                    
                    if response["data"]["has_responded"] != JSON.null {
                        let isResponsded = response["data"]["has_responded"].bool!
                        
                        if (isResponsded) {
                            // onCompletion("Thank you for your interest. You have already submitted your feedback.")
                            let long_url = response["data"]["long_url"].string!
                            onCompletion(long_url)
                        }else {
                            let long_url = response["data"]["long_url"].string!
                            onCompletion(long_url)
                        }
                    }else {
                        let long_url = response["data"]["long_url"].string!
                        onCompletion(long_url)
                    }

                }else{
                     onCompletion("No URL")
                }
            }
        }

    }
}
