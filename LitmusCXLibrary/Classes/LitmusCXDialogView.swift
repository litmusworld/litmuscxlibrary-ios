//
//  LitmusCXDialogView.swift
//  Pods
//
//  Created by admin on 03/11/17.
//
//

import UIKit

open class LitmusCXDialogView : UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
//    @objc
    public static func fnOpenLitmusFeedback(_ strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, viewController : UIViewController) {
        
        fnOpenLitmusFeedback(nil, strAppId: strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters : nil, isMoreImageBlackElseWhite : DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled : DEFAULT_COPY_ENABLED, isShareEnabled : DEFAULT_SHARE_ENABLED, viewController: viewController)
        
    }
    
//    @objc
    public static func fnOpenLitmusFeedback(_ strBaseUrl : String?, strAppId : String, nReminderNumber : Int, strUserId : String, strUserName : String, strUserEmail : String, isAllowMultipleFeedbacks : Bool, tagParameters : [String:Any]?, isMoreImageBlackElseWhite : Bool, isCopyEnabled: Bool, isShareEnabled : Bool, viewController : UIViewController) {
        
        
        let notificationPayLoad = LitmusCXView.fnCreateNotificationPayload(strBaseUrl,
                                                                                   strAppId : strAppId, nReminderNumber: nReminderNumber, strUserId: strUserId, strUserName: strUserName, strUserEmail: strUserEmail, isAllowMultipleFeedbacks: isAllowMultipleFeedbacks, tagParameters: tagParameters,
                                                                                   isMoreImageBlackElseWhite: isMoreImageBlackElseWhite, isCopyEnabled: isCopyEnabled,
                                                                       isShareEnabled: isShareEnabled)
        
        var delegate : LitmusCXDelegate?
        
        if viewController as? LitmusCXDelegate != nil {
            delegate = viewController as? LitmusCXDelegate
        }
        
        // sending delegate as nil as LitmusKLCPopup completion handler is already being called for dismissing popup
        let litmusCxView = LitmusCXView.fnGetLitmusFeedbackView(notificationPayLoad as AnyObject, delegate: nil)
        
        let mainScreenSize : CGSize = UIScreen.main.bounds.size
        
        let detailViewFrame : CGRect = CGRect(x: 20, y: 200, width: mainScreenSize.width - 40, height: mainScreenSize.height - 60)

        litmusCxView.frame = detailViewFrame
        
        let popUp = LitmusKLCPopup()
        
        popUp.contentView = litmusCxView
        
        popUp.didFinishDismissingCompletion = {
            if delegate != nil {
                delegate?.onClose()
            }
        }
        
        popUp.show()
    }
    
    
//    @objc
    public static func fnOpenWebUrl(_ strWebUrl : String, viewController : UIViewController) {
        
        fnOpenWebUrl(strWebUrl, isMoreImageBlackElseWhite: DEFAULT_MORE_IMAGE_BLACK_ELSE_WHITE, isCopyEnabled: DEFAULT_COPY_ENABLED, isShareEnabled: DEFAULT_SHARE_ENABLED, viewController: viewController)
    }

//   @objc
    public static func fnOpenWebUrl(_ strWebUrl : String, isMoreImageBlackElseWhite : Bool, isCopyEnabled: Bool, isShareEnabled : Bool , viewController : UIViewController) {
        
        var delegate : LitmusCXDelegate?
        
        if viewController as? LitmusCXDelegate != nil {
            delegate = viewController as? LitmusCXDelegate
        }
        
        // sending delegate as nil as LitmusKLCPopup completion handler is already being called for dismissing popup
        let litmusCxView = LitmusCXView.fnGetLitmusWebUrlView(strWebUrl, isMoreImageBlackElseWhite: isMoreImageBlackElseWhite, isCopyEnabled: isCopyEnabled, isShareEnabled: isShareEnabled, delegate: nil)
        
        let mainScreenSize : CGSize = UIScreen.main.bounds.size
        
        let detailViewFrame : CGRect = CGRect(x: 20, y: 30, width: mainScreenSize.width - 40, height: mainScreenSize.height - 60)
        
        litmusCxView.frame = detailViewFrame
        
        let popUp = LitmusKLCPopup()
        
        popUp.contentView = litmusCxView
        
        popUp.didFinishDismissingCompletion = {
            if delegate != nil {
                delegate?.onClose()
            }
        }
        
        popUp.show()
    }
    
}
